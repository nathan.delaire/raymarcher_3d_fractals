//
// Created by natha on 09/04/2021.
//

#pragma once

#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>


enum directions{
    FORWARD = 0,
    BACKWARD = 1,
    LEFT = 2,
    RIGHT = 3,
    UP = 4,
    DOWN = 5
};

class Camera {
public:
    Camera();

    Camera(glm::vec3 position,
           glm::vec3 direction,
           glm::vec3 worldUp);

    virtual ~Camera();

    /**
     * @param dt
     * @param direction
     * Update position vector
     */
    void updateKeyboardInput(const float &dt,
                              const int direction);

    /**
     * @param dt
     * @param offsetX
     * @param offsetY
     * Update rotations around axes (pitch, yaw and roll)
     */
    void updateMouseInput(const float &dt,
                          const double &offsetX,
                          const double &offsetY);

    /**
     * @param dt
     * @param direction
     * @param offsetX
     * @param offsetY
     * UpdateKeyboardInput and UpdateMouseInput
     */
    void updateInput(const float &dt,
                     const int direction,
                     const double &offsetX,
                     const double &offsetY);

    //Getters
    glm::mat4 getViewMatrix();
    const glm::vec3 &getPosition() const;
    const glm::vec3 &getFront() const;
    const glm::vec3 &getCamUp() const;
    const glm::vec3 &getRight() const;

    //Setters
    void setMovementSpeed(GLfloat movementSpeed);
    void setPosition(const glm::vec3 &position);
    void setFront(const glm::vec3 &front);
    void setCamUp(const glm::vec3 &camUp);
    void setRight(const glm::vec3 &right);

private:

    void updateCameraVectors();

    //Matrix of camera where we do all our translation calculus
    glm::mat4 ViewMatrix;
    //Position vectors around camera
    glm::vec3 position;
    glm::vec3 front;
    //WorldUp -> Vecteur up du monde
    //camUp -> Vecteur up de la caméra sur le moment
    glm::vec3 worldUp;
    glm::vec3 camUp;
    glm::vec3 right;

    //Rotation sur x
    GLfloat pitch;
    //Rotation sur y
    GLfloat yaw;
    //Rotation sur z
    GLfloat roll;

    //For keyboard and mouse inputs
    GLfloat movement_speed;
    GLfloat sensitivity;
};



