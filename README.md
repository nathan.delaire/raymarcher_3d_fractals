# OpenGL Raymarched 3D Fractals

A simple OpenGL real-time game engine, rendered using a Raymarcher.
Made by Nathan DELAIRE and Matthieu BOLLIAND.

Link for the demo : https://www.youtube.com/watch?v=3pMCaKEd5ck

# Requirements
In order to run de projet, you need to have :
* Cmake (for Windows and Linux)
* OpenGL (minimum version 4.4)
* GLFW 3
* GLM
* GTC (Usually installed with GLM)

# Installation

## Windows
I personnaly use MinGW in order to have a C and C++ compiler on my window 10 machine. So the option -G in the line will be "MinGW Makefiles".
Go in the projet folder then :
``
cmake  -DCMAKE_BUILD_TYPE=Release -G "CodeBlocks - MinGW Makefiles" .
``
``
cmake --build . --target Raymarching -- -j 12
``
A **Raymarching.exe** will appear on your current folder. It's the application.

## Linux

Same lines as for Windows, as long as you of course change the G option.
It's a simple cmake project building, you don't need to put any particular variables or go in a particular file.

# How to use it


## Controls

Use your keyboard and your mouse to move around the world :
* **E** : Front
* **X** : Back
* **S** : Left
* **F** : Right
* **Space** : Up
* **C** : Down
* **Z** : While pressed, you sprint

## Load a scene

In the main.cpp file, you can load several scenes by putting in the last argument of the Game constructor one of the 4 scenes we already created for you :
* sceneMandelbulb
* sceneMandelbox
* sceneGyroid
* scenePlanet
* sceneSponge

These scenes will load different fractals with different colors in the engine, feel free to explore them !
If you don't put anything in the last argument, no scenes will be loaded and a simple world with 4 shapes will appear in front of you : a Sphere, a Cube, a Torus and an Octaherdon. We don't render only fractals :)

## Create your scene

You can create a new scene or modify the one we already made for you as much as you can. Let your imagitation express itself and have fun playing with these fractals !

## Thanks to

Suraj Sharma for his OpenGL C++ tutorial series :
* https://www.youtube.com/playlist?list=PL6xSOsbVA1eYSZTKBxnoXYboy7wc4yg-Z

CodeParade for the idea of the project :
* https://www.youtube.com/watch?v=N8WWodGk9-g&t=150s

## Previews

![](./scene/sky.png)
![](./scene/mandelbulb.PNG)
![](./scene/gyroid.PNG)
![](./scene/planet.PNG)
