
#include "Engine.hpp"
#include "scene/predefined_scenes.hpp"

/**
 * Load OpenGL and its modules, creates window draws clear the window and update inputs in main loop
 */
int main()
{
    Scene *scene = scenePlanet;

    Engine engine(1920, 1080, "Fractales", sceneSponge);

    while(!glfwWindowShouldClose(engine.getWindow()))
    {
        engine.update();
        engine.render();
    }

    return 0;
}