//
// Created by natha on 08/04/2021.
//

#ifndef RAYMARCHING_ENGINE_HPP
#define RAYMARCHING_ENGINE_HPP

#include "libs.h"

class Engine {
public:
    //Constructor / Destructor
    Engine(int wWidth, int wHeight, const char *title, Scene* scene = nullptr);

    virtual ~Engine();

    void update();
    void render();

    GLFWwindow *getWindow() const;



private:

    /**
     * Init GLFW lib
     */
    void initGLFW();

    /**
    * @param window
    * @param fbW
    * @param fbH
    * Update frame when window is resized
     */
    static void framebuffer_resize_callback(GLFWwindow *window, int fbW, int fbH);

    /**
    * @param widht
    * @param height
    * @return A window configured with width and height
    */
    void initWindow();

    /**
     * Init GLEW lib
     */
    void initGLEW();

    /**
     * Set options for OpenGL
     */
    void initOpenGLOptions();
    /**
     * Init GameObjects
     */
    void initMatrices();
    void initShaders();
    void initMaterials();
    void initFrame();

    /**
     * If no scene is loaded, we load manually the object in the initObjects function
     * If the user decided to load a scene, we get the json file values in the initScene function
     */
    void initObjects();
    void initScene();

    void initLights();

    /**
     * Send Objects needed to GPU shaders
     */
    void initUniforms();

    /**
     * Update objects in GPU shaders for each frame
     */
    void updateUniforms();

    /**
    * @param window : Our current activated window
    * This function is used to move around the world using ESFX keys
    */
    void updateKeyboardInput();
    /**
     * Checks cursor movement in the app and update variables
     */
    void updateMouseInput();
    /**
     * Useful for framerate calculation
     */
    void updateDeltaTime();

    /**
     * Update our frame in front of the camera depending on the position of the camera
     */
     void updateFrame();

     /**
      * Update objects positions, if we want to animate then
      */
      void updateObjects();

    //window
    GLFWwindow *window;
    const int W_WIDTH;
    const int W_HEIGHT;
    int fbwidth;
    int fbheight;
    const char* title;

    //Delta time (movement independant from framerate)
    float dt;
    float curTime;
    float lastTime;

    //Mouse variables
    double lastMouseX;
    double lastMouseY;
    double mouseX;
    double mouseY;
    double mouseOffsetX;
    double mouseOffsetY;
    //For first call
    bool firstMouse;

    //Camera and view matrices
    glm::mat4 ViewMatrix;

    glm::vec3 camPosition;
    glm::vec3 camUp;
    glm::vec3 camFront;
    float fov;
    float nearPlane;
    float farPlane;
    glm::mat4 ProjectionMatrix;

    //Material for light reflection
    Material* material;

    //Main Shader
    Shader* shader;

    //Frame from where we'll cast all the raymarch rays
    Mesh* frame;

    //Light
    glm::vec3* light;

    //Objects in the scene
    std::vector<Object*> objects;

    //Camera
    Camera camera;

    //If we want to load a particular scene
    Scene *loadedScene;
};


#endif //RAYMARCHING_ENGINE_HPP
