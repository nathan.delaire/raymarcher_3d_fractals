//
// Created by natha on 07/04/2021.
//

#pragma once

#define PI 3.14

#include <GL/glew.h>
#include <GLFW/glfw3.h>


#include <vector>
#include <string>
#include "Vertex.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/mat4x4.hpp>


class Primitive {
public:
    Primitive();
    virtual ~Primitive();

    /**
     * @param vertices
     * @param nbVertices
     * @param indices
     * @param nbIndices
     * Fill the classes vectors according to the parameters
     */
    void set(const Vertex *vertices,
             unsigned nbVertices,
             const GLuint *indices,
             unsigned nbIndices);

    //Getters
    Vertex *getVertices();
    unsigned getNbVertices();

    GLuint *getIndices();
    unsigned getNbIndices();
    const glm::vec3 &getCenter() const;

private:
    std::vector<Vertex> vertices;
    std::vector<GLuint> indices;

protected:
    glm::vec3 center;
};

/**
 * Quad : Two meshes representing
 */
class Quad : public Primitive{
public:
    Quad(glm::vec3 camPos, float nearPlane, glm::mat4 ViewMatrix);
};
