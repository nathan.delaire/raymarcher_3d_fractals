//GLSL version
#version 440

#define MAX_OBJ 10

//3D Object class
struct Object
{
    vec3 center;
    float size;

    //0 = sphere
    //1 = cube
    //2 = torus
    //3 = octahedron
    //4 = mandlebulb
    //5 = gyroid
    //6 = infinite planets
    //7 = mandelbox
    //8 = sponge
    int type;

    //Just for mandlebulb and sponge
    float iterations;

    //Mandelbulb
    float bailout;
    float power;

    //Just for gyroid and sponge
    float scale;

    //Gyroid
    float thickness;
    float biais;
};

//Material class
struct Material
{
    //RGBs
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

//What will come in (from what vertex core program did send out)
in vec3 vs_position;

//What will come out (a fragment -> a pixel)
out vec4 fs_color;




// ============================================ UNIFORMS =========================================================== ///


//Material class (where material class attributes will be send in)
uniform Material material;


//Light position loading
uniform vec3 lightPos0;

//Camera position for specular lighting calculation
uniform vec3 camPosition;

//Camera front vector
uniform vec3 camFront;

uniform int NB_OBJ;

//Objects in the scene
uniform Object objects[MAX_OBJ];

//Color of the objects
uniform vec3 obj_color;

//Maximum of ray advancements
uniform float MAX_RAY_STEPS;

//Minimum length of ray (when it hurts something)
uniform float MIN_DISTANCE;

//Maximum length of ray
uniform float MAX_DISTANCE;

uniform bool renderGlow;
uniform bool renderAO;
uniform bool renderNormals;


// ========================================= LIGHTS CALCULATIONS =================================================== ///



//Functions
vec4 calculateAmbient(Material material)
{
    //Last is for alpha
    return vec4(material.ambient, 1.f);
}

vec4 calculateDiffuse(Material material, vec3 lightPos0, vec3 normal, vec3 position)
{
    vec3 dir = normalize(lightPos0 - position);
    vec3 diffuse_color = material.diffuse;

    //We clamp value between 0 and 1
    float diffuse = clamp(dot(dir, normal), 0, 1);
    vec3 diffuseVec = diffuse_color * diffuse;

    return vec4(diffuseVec, 1.f);
}

vec4 calculateSpecular(Material material, vec3 lightPos0, vec3 normal, vec3 position)
{
    vec3 lightToPosDirVec = normalize(position - lightPos0);

    //reflect is a GLSL function
    vec3 reflectDirVec = normalize(reflect(lightToPosDirVec, normalize(normal)));
    vec3 posToViewDirVec = normalize(camPosition - position );

    //Look at specular light formula, 30 est la composante ns
    float specularConstant = pow(max(dot(posToViewDirVec, reflectDirVec), 0), 30);
    vec3 specularFinal = material.specular * specularConstant;

    return vec4(specularFinal, 1.f);
}






// ===================================== DISTANCE ESTIMATORS (SDF) ================================================= ///


//Just cubic fractal
vec3 boxFold(vec3 z, vec3 r) {
	return clamp(z, -r, r) * 2.0 - z;
}

//Spheric fractals
void sphereFold(inout vec3 z, inout float dz) {

    float fixedRadius2 = 0.6f + 4.f * cos(20.f / 8.f) + 4.f;
    float minRadius2 = 0.3f;

	float r2 = dot(z,z);

	if (r2 < minRadius2) {
		float temp = fixedRadius2 / minRadius2;
		z *= temp;
		dz *= temp;
	}
    else if (r2 < fixedRadius2) {
		float temp = fixedRadius2 / r2;
		z *= temp;
		dz *= temp;
	}
}


float mandleBoxDE(vec3 pos, Object mandle)
{
    pos = pos - mandle.center;
    vec3 z = pos;

    float Iterations = mandle.iterations;
    float Scale = mandle.scale;

    //derivative, same as mandlebulb
	float dr = 1.0f;

	for (float n = 0.f; n < Iterations; n++) {

        z = boxFold(z, vec3(2.0f));
        //z.xz = -z.zx;

		z = boxFold(z, vec3(1.f));
		sphereFold(z, dr);

        z = Scale * z + pos;
        //derivate z
        dr = dr * abs(Scale) + 1.f;
	}
	float r = length(z);
	return r / abs(dr);
}


float mandleDistanceEstimator(vec3 pos, Object mandle) {

	//First line is for (possible ?) infinite mandlebulb (TODO)

	//pos = mod(pos - mandle.center + 0.5f * mandle.center, mandle.center) - 0.5f * mandle.center;
	pos = pos - mandle.center;
	vec3 z = pos;
	float dr = 1.f;
	float r = 0.f;

    //Get values from object
	float Iterations = mandle.iterations;
	float Bailout = mandle.bailout;
	float Power = mandle.power;

	for (int i = 0; i < Iterations ; i++) {
		r = length(z);
		if (r > Bailout) break;

		//Convert x, z, z to polar values so that we can work on it
		float theta = acos(z.z / r);
		float phi = atan(z.y, z.x);

		float zr = pow(r , Power);
		//derivative of zr
		dr = pow(r, Power - 1.f) * Power * dr + 1.f;

		// scale and rotate the point
		theta = theta * Power;
		phi = phi * Power * 2.f;

		// convert back to cartesian coordinates
		z = zr * vec3(sin(theta) * cos(phi), sin(phi) * sin(theta), cos(theta));
		z += pos;
	}

	//Distance estimator for 2D complex Mandelbrot
	//dr is the length of the complex derivative
	return 0.5 * log(r) * r / dr;
}

float genGyroid(vec3 p, float scale, float thickness, float biais)
{
    p *= scale;
    return abs(dot(sin(p), cos(p.zxy)) - biais) / scale - thickness;
}

float gyroidDistanceEstimator(vec3 p, Object gyro)
{
    float g1 = genGyroid(p, gyro.scale, gyro.thickness, gyro.biais);
    return g1;
}

float infiniteDistanceEstimator(vec3 p, Object object)
{
    return dot(sin(p), sin(p)) * 0.5f;
}


float sphereDistanceEstimator(vec3 p, Object sphere)
{
    return length(p - sphere.center) - sphere.size;
}

float cubeDistanceEstimator(vec3 p, Object cube)
{
    vec3 b = vec3(cube.size);
    vec3 q = abs(p - cube.center) - b;
    return length(max(q, 0.0)) + min(max(q.x, max(q.y, q.z)), 0.0);
}

float torusDistanceEstimator(vec3 p, Object torus)
{
    p = p - torus.center;
    vec3 t = vec3(torus.size, 2.f, 2.f);
    vec2 q = vec2(length(p.xz) - t.x, p.y);
    return length(q) - t.y;
}

//Not exact, but close enough
float octahedronDistanceEstimator(vec3 pos, Object octa)
{
    pos = abs(pos - octa.center);
    return (pos.x + pos.y + pos.z - octa.size) * 0.57735027;
}

float spongeDistanceEstimator(vec3 p, Object cube){
   float d = gyroidDistanceEstimator(p, cube);

   float div = 0.3;
   float scale = cube.scale;
   float Iterations = cube.iterations;
   for( int m = 0; m < Iterations; m++ )
   {
      vec3 a = mod( p * div, 2.0 )-1.0;
      div *= 3.0;
      vec3 r = abs(1.0 - scale * abs(a));

      float da = max(r.x,r.y);
      float db = max(r.y,r.z);
      float dc = max(r.z,r.x);
      float c = (min(da,min(db,dc))-1.0) / div;

      if( c >= d )
          d = c;

   }
   return d;
}


float objectDistanceEstimator(vec3 p, Object object)
{
    if (object.type == 0)
        return sphereDistanceEstimator(p, object);
    if (object.type == 1)
        return spongeDistanceEstimator(p, object);
    if (object.type == 2)
        return torusDistanceEstimator(p, object);
    if (object.type == 3)
        return octahedronDistanceEstimator(p, object);
    if (object.type == 4)
        return mandleDistanceEstimator(p, object);
    if (object.type == 5)
        return gyroidDistanceEstimator(p, object);
    if(object.type == 6)
        return infiniteDistanceEstimator(p, object);
    if(object.type == 7)
        return mandleBoxDE(p, object);
    if(object.type == 8)
        return spongeDistanceEstimator(p, object);
    else
        return MAX_DISTANCE;
}

float DistanceEstimator(vec3 p)
{
    if (NB_OBJ < 1) return 0.;
    float min_dst = objectDistanceEstimator(p, objects[0]);
    for(int i = 1; i < NB_OBJ; i++)
    {
        min_dst = min(min_dst, objectDistanceEstimator(p, objects[i]));
    }
    return min_dst;
}




// ============================================== RAYMARCHER ======================================================= ///


//Approximate normal, not the exact value
vec3 GetNormal(vec3 point)
{
    float dst = DistanceEstimator(point);

    vec3 normal = dst - vec3(
        DistanceEstimator(point - vec3(0.01f, 0.f, 0.f)),
        DistanceEstimator(point - vec3(0.f, 0.01f, 0.f)),
        DistanceEstimator(point - vec3(0.f, 0.f, .01f)));

    return normalize(normal);
}

//from = cameraPosition
//direction = vector between cameraPosition and point on frame
vec2 march(vec3 from, vec3 direction)
{
    float totalDistance = 0.f;
    int steps = 0;

    //Each steps is when we move on the ray
    for(; steps < MAX_RAY_STEPS; steps++){

        vec3 p = from + totalDistance * direction;

        //Distance estimator estimates how far we have to advance the ray
        float distance = DistanceEstimator(p);
        totalDistance += distance;

        //if distance is absolutely low, it means that we hurted something
        if(distance < MIN_DISTANCE || distance > MAX_DISTANCE) break;
    }

    return vec2(totalDistance, steps);
}

//Ambient occlusion method
//Occlusion to ambient light
float ao(vec3 p, vec3 n) {

    float occ = 0.0f;
    float epsilon = 0.1f;
    vec3 u = vec3(0.f);

    //On fait tourner le vecteur n
    if (abs(n.x) < abs(n.y) && abs(n.x) < abs(n.z)) {
        u = cross(n, vec3(1,0,0));
    }
    else if (abs(n.y) < abs(n.z)) {
        u = cross(n, vec3(0,1,0));
    }
    else {
        u = cross(n, vec3(0,0,1));
    }

    vec3 v = cross(n, u);

    //On approche la formule de l'intégrale
    occ += max(DistanceEstimator(p + epsilon * n), 0.f);
    occ += max(DistanceEstimator(p + epsilon * u), 0.f);
    occ += max(DistanceEstimator(p + epsilon * (-u)), 0.f);
    occ += max(DistanceEstimator(p + epsilon * v), 0.f);
    occ += max(DistanceEstimator(p + epsilon * (-v)), 0.f);
    occ += max(DistanceEstimator(p + 4.0 * epsilon * (n + u)) / 2.f, 0.f);
    occ += max(DistanceEstimator(p + 4.0 * epsilon * (n - u)) / 2.f, 0.f);
    occ += max(DistanceEstimator(p + 4.0 * epsilon * (n + v)) / 2.f, 0.f);
    occ += max(DistanceEstimator(p + 4.0 * epsilon * (n - v)) / 2.f, 0.f);
    occ += max(DistanceEstimator(p + 2.0 * epsilon * n) / sqrt(2.f), 0.f);
    occ += max(DistanceEstimator(p + 3.0 * epsilon * n) / sqrt(3.f), 0.f);
    occ += max(DistanceEstimator(p + 4.0 * epsilon * n) / sqrt(4.f), 0.f);

    return clamp(1.0 - 1.0 / ( 1.0 + 2.0 * occ), 0.0, 1.0);
}

vec4 GetLight(vec3 point) {
    vec3 l = normalize(lightPos0 - point);
    vec3 n = GetNormal(point);

    vec4 ambiant = calculateAmbient(material);
    vec4 diffuse = calculateDiffuse(material, lightPos0, n, point);
    vec4 specular = calculateSpecular(material, lightPos0, n, point);

    return diffuse + ambiant + specular;
}


void main()
{
    //Get vector betweeen camPosition and position in our frame
    vec3 direction = vs_position.xyz - camPosition;

    //Raymarch (x = distance, y = nb_steps)
    vec2 dist_steps = march(camPosition, direction);
    float distance = dist_steps.x;
    float steps = dist_steps.y;

    //If we hurt something
    if(distance <= MAX_DISTANCE)
    {
        vec3 p = camPosition + direction * distance;
        vec4 light_apport = GetLight(p);
        vec3 n = GetNormal(p);

        //Several colors we can apply to our objects
        vec4 normal_color = vec4(n, 1.f);

        //vec4 color2 = vec4(0.63f, 0.06f, 0.63f, 1.f);
        //vec4 color3 = vec4(0.86f, 0.12f, 0.06f, 1.f);
        //vec4 color4 = vec4(0.06f, 0.12f, 0.76f, 1.f);
        //vec4 color5 = vec4(0.9f, 0.59f, 0.21f, 1.f);
        //vec4 color6 = vec4(0.12f, 0.86f, 0.06f, 1.f);
        //vec4 color7 = vec4(0.86f, 0.12f, 0.06f, 1.f);

        //Glow lighting effect depending on the number of steps
        vec4 glow = vec4(vec3(steps) / 20, 1.f);

        //Ambient occlusion
        float OA = ao(p, n);


        //Different return values
        fs_color = light_apport * vec4(obj_color, 1.f);

        if(renderGlow)
            fs_color *= glow;
        if(renderAO)
            fs_color *= OA;
        if(renderNormals)
            fs_color *= normal_color;

    }
    else
        fs_color = vec4(0.f);
}